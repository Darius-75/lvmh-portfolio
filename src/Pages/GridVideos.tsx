import React from "react";
import styled from "styled-components";
import VVideo from "../videos/da.mp4";
import video2 from "../videos/An Inside Look at the SEE LV Exhibition in Tokyo  LOUIS VUITTON (2).mp4";
import video3 from "../videos/Hoyeon and the Twist  the new chapter  LOUIS VUITTON.mp4";
import video4 from "../videos/The Holiday Season with Louis Vuitton.mp4";
import MediaQuery from "react-responsive";
import { colors, font, size } from "../Variables/globalStyles";
import CmsSectionRight from "../components/molecules/CmsSections/CmsSectionHoliday";
import CmsSectionTwist from "../components/molecules/CmsSections/CmsSectionTwist";
import CmsSectionCapucine from "../components/molecules/CmsSections/CmsSectionCapucine";
import CmsSectionSeelv from "../components/molecules/CmsSections/CmsSectionSeelv";
import CmsSectionHoliday from "../components/molecules/CmsSections/CmsSectionHoliday";
import { Link } from "react-scroll";
import { useNavigate } from "react-router";
import { useEffect } from "react";
import { useLocation } from "react-router-dom";
import ScrollIcon from "../img/cmsScrollIcon.png";
import Icon from "../components/atoms/Icon/Icon";

type Props = {};
const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const GridVideos = (props: Props) => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return (
    <>
      <MediaQuery maxWidth={1000}>
        <div id="toop"></div>
        <Grid>
          <Video src={VVideo} autoPlay loop muted />
          <Video src={video2} autoPlay loop muted />
          <Video src={video3} autoPlay loop muted />
          <Video src={video4} autoPlay loop muted />
          <Link
            activeClass="active"
            to="cms1"
            spy={true}
            smooth={true}
            offset={50}
            duration={500}
          >
            <ScollBarSection>▽</ScollBarSection>
          </Link>
        </Grid>
        <div id="cms1"></div>
        <CmsSectionTwist />
        <CmsSectionCapucine />
        <CmsSectionSeelv />
        <CmsSectionHoliday />
      </MediaQuery>
      <MediaQuery minWidth={1001}>
        <Grid>
          <Video src={VVideo} autoPlay loop muted />
          <Video src={video2} autoPlay loop muted />
          <Video src={video3} autoPlay loop muted />
          <Video src={video4} autoPlay loop muted />
          <ScrollHomeSectionButton>
            <Link
              activeClass="active"
              to="#TwistSection"
              spy={true}
              smooth={true}
              offset={1}
              duration={600}
            >
              <Icon src={ScrollIcon} width={"50px"} height={"50px"} />
            </Link>
          </ScrollHomeSectionButton>
        </Grid>
        <div id="#TwistSection" />
        <CmsSectionTwist />
        <CmsSectionCapucine />

        <CmsSectionSeelv />
        <CmsSectionHoliday />
      </MediaQuery>
    </>
  );
};

const Grid = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
  display: grid;
  overflow: hidden;
  @media screen and (min-width: 1000px) {
    position: relative;
    height: 100%;
    width: 100%;
    display: grid;
    overflow: hidden;
    grid-template-columns: 1fr 1fr;
  }
`;

const Video = styled.video`
  width: 100%;
  overflow: hidden;
  @media screen and (min-width: 1000px) {
    width: 100%;
    overflow: hidden;
  }
`;

const ScollBarSection = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  color: #ffff;
  background-color: black; /* couleur de départ transparente */
  transition: background-color 0.5s ease-out;
  cursor: pointer;
  z-index: 1;
  &:hover {
    color: #000;
    background-color: ${Secondary}; /* couleur de fin */
    background-image: linear-gradient(to bottom, transparent, ${Secondary});
  }
`;

const ScrollHomeSectionButton = styled.div`
  @media screen and (min-width: 1000px) {
    width: 100%;
    height: 100%;
    position: absolute;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
  }
`;

export default GridVideos;
