import React from 'react'
import styled from 'styled-components'
import HomeContent from '../components/Pagecontent/HomeContent/HomeContent'
import Navbar from '../components/Pagecontent/Navbar/Navbar'
import AboutPage from './AboutPage'
import FooterPage from './FooterPage'
import PortfolioPage from './PortfolioPage'
import SkillsPage from './SkillsPage'

const DisplayPages = styled.div`
display: flex;
flex-direction: column;

`
const GlobalPage = () => {
  return (
    <div>
        <DisplayPages>
          <Navbar/>
        <HomeContent/>
        <AboutPage/>
        <PortfolioPage/>
        <SkillsPage/>
        <FooterPage/>
        </DisplayPages>
   </div>
  )
}

export default GlobalPage