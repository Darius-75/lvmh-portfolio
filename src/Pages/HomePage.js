import HomeContent from '../components/Pagecontent/HomeContent/HomeContent'
import Navbar from '../components/Pagecontent/Navbar/Navbar'

const HomePage = () => {
  return (
    <div> 
        <Navbar/>
        <HomeContent/>
    </div>
  )
}

export default HomePage