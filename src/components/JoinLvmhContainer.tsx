import React from "react";
import HorizontalScroll from "react-scroll-horizontal";
import styled from "styled-components";
import JoinLvmhContent from "./Pagecontent/JoinLvmContent/JoinLvmhContent";

type Props = {};

const JoinLvmhContainer = (props: Props) => {
  return (
    // <DA>
    <>
      <Content>
        <HorizontalScroll>
          <POP>
            <JoinLvmhContent />
          </POP>
          <D>
            <h1>Hello 1</h1>
          </D>
          <D>
            <h1>Hello 2</h1>
          </D>
          <D>
            <h1>Hello 3</h1>
          </D>{" "}
          <D>
            <h1>Hello 1</h1>
          </D>
          <D>
            <h1>Hello 2</h1>
          </D>
          <D>
            <h1>Hello 3</h1>
          </D>
        </HorizontalScroll>
      </Content>
    </>
  );
};

const D = styled.div`
  @media screen and (min-width: 1000px) {
    width: 30em;
    height: 100vh;
    background-color: red;
    border: black 2px solid;
  }
`;

const Content = styled.div`
  @media screen and (min-width: 1000px) {
    width: 100%;
    height: 100%;
  }
`;
const POP = styled.div`
  @media screen and (min-width: 1000px) {
    width: 215vh;
    height: 100%;
  }
`;

export default JoinLvmhContainer;
