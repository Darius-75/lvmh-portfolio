import { AiOutlineInstagram, AiOutlineLinkedin } from "react-icons/ai";
import { IoNewspaperOutline } from "react-icons/io5";
import { IoMdPaper } from "react-icons/io";
import styled from "styled-components";
import { colors, size, font } from "../../../Variables/globalStyles";
import Spacer from "../../atoms/Spacer/Spacer";
import Text from "../../atoms/Text/Text";
import Line from "../../atoms/Line/Line";
import MediaQuery from "react-responsive";

type Props = {};

const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const GlobalFooterDisplay = styled.div`
  height: max-content;
  width: 100%;
  background-color: #000;
  display: flex;
  flex-direction: column;
  align-items: center;
  font-family: ${normal};
`;

const SectionDisplay = styled.div`
  display: flex;
  height: max-content;
  flex-direction: column;
  width: 100%;
  align-items: center;
  @media screen and (min-width: 1001px) {
    flex-direction: column;
  }
`;

const IconsDisplay = styled.div`
  display: flex;
  height: max-content;
  width: 100px;
  padding-top: 10px;
  justify-content: space-around;
`;

const SectionDisplayContainer = styled.div`
  @media screen and (min-width: 1001px) {
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    width: 50%;
  }
`;

const FooterContent = (props: Props) => {
  return (
    <>
      <MediaQuery maxWidth={1000}>
        <div id="contact"></div>
        <GlobalFooterDisplay>
          <Spacer height="50px" />

          <Text value="AMINE ABBES" size={Max} color="#fff" />
          <Spacer height="30px" />

          <SectionDisplay>
            <Text value="Réseaux Pro" color="#fff" />
            <IconsDisplay color="#fff">
              <AiOutlineLinkedin
                onClick={() =>
                  window.open(
                    "https://www.linkedin.com/in/amine-abbes-8207411a4/",
                    "_blank"
                  )
                }
                color="#fff"
                size={"40px"}
              />

              <AiOutlineInstagram
                color="#fff"
                size={"40px"}
                onClick={() =>
                  window.open(
                    "file:///C:/Users/Darius/Downloads/CV_Amine_Abbes_PPT%202.pdf.pdf",
                    "_blank"
                  )
                }
              />
            </IconsDisplay>
          </SectionDisplay>

          <Spacer height="25px" />
          <Line background="#DFD9D9" width="80%" height="0.1555555px" />
          <Spacer height="25px" />

          <SectionDisplay>
            <Text value="CV Pro" color="#fff" />
            <IconsDisplay
              color="#fff"
              onClick={() =>
                window.open(
                  "https://7b015da1-c991-46c2-8e76-9c9564167e2d.filesusr.com/ugd/052fa8_e415e8ec3cb44555a8dddd3f83e43d8b.pptx?dn=CV_Amine_Abbes_PPT%20(1).pptx",
                  "_blank"
                )
              }
            >
              <IoMdPaper
                color="#fff"
                size={"40px"}
                onClick={() =>
                  window.open(
                    "https://7b015da1-c991-46c2-8e76-9c9564167e2d.filesusr.com/ugd/052fa8_e415e8ec3cb44555a8dddd3f83e43d8b.pptx?dn=CV_Amine_Abbes_PPT%20(1).pptx",
                    "_blank"
                  )
                }
              />
              <IoNewspaperOutline color="#fff" size={"40px"} />
            </IconsDisplay>
          </SectionDisplay>
          <Spacer height="50px" />
        </GlobalFooterDisplay>
      </MediaQuery>

      <MediaQuery minWidth={1001}>
        <div id="footer"></div>

        <GlobalFooterDisplay>
          <Spacer height="20px" />

          <Text value="AMINE ABBES" size={Max} color="#fff" />
          <Spacer height="20px" />

          <SectionDisplayContainer>
            <SectionDisplay>
              <Text value="Réseaux Pro" color="#fff" />
              <IconsDisplay color="#fff">
                <AiOutlineLinkedin
                  onClick={() =>
                    window.open(
                      "https://www.linkedin.com/in/amine-abbes-8207411a4/",
                      "_blank"
                    )
                  }
                  color="#fff"
                  size={"40px"}
                />
                <Spacer height="30px" />

                <AiOutlineInstagram
                  color="#fff"
                  size={"40px"}
                  onClick={() =>
                    window.open(
                      "file:///C:/Users/Darius/Downloads/CV_Amine_Abbes_PPT%202.pdf.pdf",
                      "_blank"
                    )
                  }
                />
              </IconsDisplay>
            </SectionDisplay>
            <Line background="#DFD9D9" width="80%" height="0.1555555px" />

            <SectionDisplay>
              <Text value="CV Pro" color="#fff" />
              <IconsDisplay
                color="#fff"
                onClick={() =>
                  window.open(
                    "https://7b015da1-c991-46c2-8e76-9c9564167e2d.filesusr.com/ugd/052fa8_e415e8ec3cb44555a8dddd3f83e43d8b.pptx?dn=CV_Amine_Abbes_PPT%20(1).pptx",
                    "_blank"
                  )
                }
              >
                <IoMdPaper
                  color="#fff"
                  size={"40px"}
                  onClick={() =>
                    window.open(
                      "https://7b015da1-c991-46c2-8e76-9c9564167e2d.filesusr.com/ugd/052fa8_e415e8ec3cb44555a8dddd3f83e43d8b.pptx?dn=CV_Amine_Abbes_PPT%20(1).pptx",
                      "_blank"
                    )
                  }
                />
                <IoNewspaperOutline color="#fff" size={"40px"} />
              </IconsDisplay>
            </SectionDisplay>
          </SectionDisplayContainer>
          <Spacer height="50px" />
        </GlobalFooterDisplay>
      </MediaQuery>
    </>
  );
};

export default FooterContent;
