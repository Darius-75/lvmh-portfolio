import React, { useEffect, useRef } from "react";
import styled from "styled-components";
import Icon from "../../atoms/Icon/Icon";
import Text from "../../atoms/Text/Text";
import lvCms from "../../../img/lvCms.jpg";
import lvDesign from "../../../img/lvDesign.jpg";
import lvCertifimg from "../../../img/lvCertifimg.png";
import Spacer from "../../atoms/Spacer/Spacer";
import { colors, font, size } from "../../../Variables/globalStyles";
import MediaQuery from "react-responsive";
import { useNavigate } from "react-router-dom";
import GridVideos from "../../../Pages/GridVideos";
import { Link } from "react-scroll";

type Props = {};
const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const GlobalPortfolioDisplayPages = styled.div`
  height: max-content;
  width: 100%;
  background-color: ${Primary};
  padding: 0px 30px;
  display: flex;
  flex-direction: column;
  font-family: ${normal};
`;

const GlobalPortfolioDisplaySection = styled.div`
  height: max-content;
  width: 100%;
  background-color: ${Primary};
  padding: 0px 30px;
  display: flex;
  flex-direction: column;
  font-family: ${normal};
  @media screen and (min-width: 1001px) {
    display: flex;
    width: 100%;
    padding: 0px;
    flex-direction: row;
    justify-content: flex-start;
    align-content: flex-start;
  }
`;

const SectionContainer = styled.div`
  height: max-content;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 100%;

  @media screen and (min-width: 1001px) {
    display: flex;
    width: 100%;
    align-items: flex-start;
    padding: 10px;
  }
`;

const SectionContent = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
  @media screen and (min-width: 1001px) {
    align-items: flex-start;
    justify-content: center;
    margin: 5px;
    height: 500px;
    padding: 20px;
  }
`;

const AA = styled.div`
  display: flex;
  align-items: flex-start;
  width: 100%;
`;

function RedirectToTop() {
  const navigate = useNavigate();

  function HandleClick() {
    navigate("/cms  ");
  }

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return HandleClick;
}

const PortfolioContent = (props: Props) => {
  const HandleClick = RedirectToTop();
  return (
    <>
      <MediaQuery maxWidth={1000}>
        <GlobalPortfolioDisplayPages>
          <Spacer height="30px" />
          <Text value="A DECOUVRIR EGALEMENT" size={Max} />
          <Spacer height="30px" />

          <SectionContainer>
            <SectionContent>
              <Icon
                src={lvCms}
                height="min-height"
                width="100%"
                handleClick={HandleClick}
              />
            </SectionContent>
            <AA>
              <Text fontWeight="600" padding="15px 0 0 0" value="Cms . . ." />
            </AA>
          </SectionContainer>

          <SectionContainer>
            <Spacer height="50px" />

            <SectionContent>
              <Icon
                src={lvDesign}
                height="100%"
                width="100%"
                handleClick={() =>
                  window.open(
                    "https://www.figma.com/file/d7qQ3BztdNX3583GOTgOvK/Untitled?t=eKzKQwZx7llI8aLL-6",
                    "_blank"
                  )
                }
              />
            </SectionContent>
            <AA>
              <Text
                fontWeight="600"
                padding="15px 0 0 0"
                value="Designs . . ."
              />
            </AA>
          </SectionContainer>

          <SectionContainer>
            <Spacer height="50px" />

            <SectionContent>
              <Icon
                src={lvCertifimg}
                height="min-height"
                width="100%"
                handleClick={() =>
                  window.open(
                    "https://amineestiam75.wixsite.com/lvmhportfolioshare",
                    "_blank"
                  )
                }
              />
            </SectionContent>
            <AA>
              <Text
                fontWeight="600"
                padding="15px 0 0 0"
                value="Certifications . . ."
              />
            </AA>
          </SectionContainer>
          <Spacer height="20px" />
        </GlobalPortfolioDisplayPages>
      </MediaQuery>

      <MediaQuery minWidth={1001}>
        <GlobalPortfolioDisplayPages>
          <Spacer height="20px" />
          <Text value="A DECOUVRIR EGALEMENT" size={Max} />
          <Spacer height="30px" />
          <GlobalPortfolioDisplaySection>
            <SectionContent>
              <Icon
                src={lvCms}
                height="70%"
                width="100%"
                handleClick={() =>
                  window.open(
                    "https://amineestiam75.wixsite.com/lvmhportfolioshare",
                    "_blank"
                  )
                }
              />
              <Text fontWeight="600" padding="15px 0 0 0" value="Cms . . ." />
            </SectionContent>
            <SectionContent>
              <Icon
                src={lvDesign}
                height="70%"
                width="100%"
                handleClick={() =>
                  window.open(
                    "https://www.figma.com/file/d7qQ3BztdNX3583GOTgOvK/Untitled?t=eKzKQwZx7llI8aLL-6",
                    "_blank"
                  )
                }
              />
              <Text
                fontWeight="600"
                padding="15px 0 0 0"
                value="Designs . . ."
              />
            </SectionContent>
            <SectionContent>
              <Icon
                src={lvCertifimg}
                height="70%"
                width="100%"
                handleClick={() =>
                  window.open(
                    "https://amineestiam75.wixsite.com/lvmhportfolioshare",
                    "_blank"
                  )
                }
              />
              <Text
                fontWeight="600"
                padding="15px 0 0 0"
                value="Certifications . . ."
              />
            </SectionContent>
          </GlobalPortfolioDisplaySection>
          <Spacer height="20px" />
        </GlobalPortfolioDisplayPages>
      </MediaQuery>
    </>
  );
};

export default PortfolioContent;
