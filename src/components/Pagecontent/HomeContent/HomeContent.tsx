import styled from "styled-components";
import { colors, size, font } from "../../../Variables/globalStyles";
import ProfilPic from "../../../img/ppf.png";
import reactsIcon from "../../../img/logotsx.png";
import { AiOutlineHeart } from "react-icons/ai";
import { HiOutlineDotsCircleHorizontal } from "react-icons/hi";
import nodeIcon from "../../../img/nodeIcon.png";
import mongodbIcon from "../../../img/mongodbIcon.png";
import dockerIcon from "../../../img/dockerIcon.png";
import Line from "../../atoms/Line/Line";
import Button from "../../atoms/Button/Button";
import Text from "../../atoms/Text/Text";
import Icon from "../../../components/atoms/Icon/Icon";
import { Link } from "react-scroll";
import MediaQuery from "react-responsive";
import { IoBookmarkOutline } from "react-icons/io5";
type Props = {};

const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const GlobalStyledHome = styled.div`
  font-family: ${normal};
  @media screen and (min-width: 1000px) {
    display: flex;
    height: 86vh;
    align-items: center;
    background-color: #fff;
  }
`;

const TopStyledHome = styled.div`
  width: 100%;
  height: 400px;
  background: ${Primary};
  position: relative;
  display: flex;
  justify-content: space-around;
  @media screen and (min-width: 1000px) {
    width: 50%;
    height: 100%;
  }
`;

const ImgPosition = styled.div`
  padding-top: 30px;
  padding-left: 10px;
  @media screen and (min-width: 1001px) {
    width: 50%;
    height: 100%;
    padding-bottom: 0px;
    display: flex;
    align-items: flex-end;
  }
`;

const IconPosition = styled.div`
  display: flex;
  flex-direction: column;
  padding: 20px 10px 10px 20px;
  justify-content: space-between;
`;

const BotStyledHome = styled.div`
  padding: 23px;

  @media screen and (min-width: 1000px) {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 50%;
  }
`;

const BotStyledContent = styled.div`
  padding: 23px;
  height: max-content;

  @media screen and (min-width: 1000px) {
    height: max-content;
    width: max-content;
  }
`;

const BottomHeartIconPosition = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;

const BottomTextandDetailIconPosition = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  @media screen and (min-width: 1000px) {
    display: flex;
    justify-content: start;
    align-items: center;
  }
`;

const ColorSection = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const HomeContent = (props: Props) => {
  return (
    <div>
      <MediaQuery maxWidth={1000}>
        <GlobalStyledHome>
          <TopStyledHome>
            <ImgPosition>
              <Icon src={ProfilPic} height="100%" width="100%" />
            </ImgPosition>
            <IconPosition>
              <Icon src={reactsIcon} height="65px" width="65px" />
              <Icon src={nodeIcon} height="65px" width="65px" />
              <Icon src={mongodbIcon} height="65px" width="65px" />
              <Icon src={dockerIcon} height="65px" width="65px" />
            </IconPosition>
          </TopStyledHome>
          <BotStyledHome>
            <BottomHeartIconPosition>
              <Text value={"ALTERNANT2023"} size={Min} padding="0 0 10px 0" />
              <AiOutlineHeart />
            </BottomHeartIconPosition>
            <Text value={"ABBES AMINE"} size={Max} padding=" 3px 0" />
            <Text value={"DEVELOPPEUR  UI-UX "} size={Max} padding=" 3px 0" />
            <Text
              value={" FULL-STACK / E-commerce "}
              size={Max}
              padding=" 2px 0"
            />

            <Line
              background="#DFD9D9"
              width="100%"
              height="1px"
              margin="10px 0 0 0"
            />

            <ColorSection>
              <Text
                value={"Couleurs"}
                padding="
          15px 0"
              />
              <Text value={"Full-stack"} />
            </ColorSection>
            <Line background="#DFD9D9" width="100%" height="1px" />
            <Link
              activeClass="active"
              to="footer"
              spy={true}
              smooth={true}
              offset={120}
              duration={2000}
            >
              <Button
                color="#001"
                width="100%"
                height="50px"
                margin="20px 0 20px 0"
                display="flex"
                textAlign="center"
                alignItems="center"
                justifyContent="center"
              >
                <Text value={"Ajouter à l'entreprise"} color={"#fff"} />
              </Button>
            </Link>
            <Text
              value={
                "Le délai d’éxédition de ce produit est estimé entre 0 et 1 jours."
              }
              padding="
            0 0 10px 0"
            />
            <Text
              value={"Livraison sur-mesure dès demain sur Paris"}
              padding="
            0 0 10px 0"
            />
            <Link
              activeClass="active"
              to="test"
              spy={true}
              smooth={true}
              offset={50}
              duration={500}
            >
              <Line
                background="#DFD9D9"
                width="100%"
                height="1px"
                margin="20px 0 0 0"
              />
              <BottomTextandDetailIconPosition>
                <HiOutlineDotsCircleHorizontal size={"20px"} />
                <Text value={"Détails"} margin="  16px 0  16px 8px " />
                <div id="test" />
              </BottomTextandDetailIconPosition>
              <Line background="#DFD9D9" width="100%" height="1px" />
            </Link>
          </BotStyledHome>
        </GlobalStyledHome>
      </MediaQuery>

      {/* responsive pc */}

      <MediaQuery minWidth={1001}>
        <GlobalStyledHome>
          <TopStyledHome>
            <ImgPosition>
              <Icon src={ProfilPic} height="80%" width="100%" />
            </ImgPosition>
            <IconPosition>
              <Icon src={reactsIcon} height="65px" width="65px" />
              <Icon src={nodeIcon} height="65px" width="65px" />
              <Icon src={mongodbIcon} height="65px" width="65px" />
              <Icon src={dockerIcon} height="65px" width="65px" />
            </IconPosition>
          </TopStyledHome>
          <BotStyledHome>
            <BotStyledContent>
              <BottomHeartIconPosition>
                <Text value={"ALTERNANT2023"} size={Min} padding="0 0 10px 0" />
                <IoBookmarkOutline cursor={"pointer"} />
              </BottomHeartIconPosition>
              <Text value={"ABBES AMINE"} size={Max} padding=" 3px 0" />
              <Text value={"DEVELOPPEUR  UI-UX "} size={Max} padding=" 3px 0" />
              <Text
                value={" FULL-STACK / E-commerce "}
                size={Max}
                padding=" 2px 0"
              />

              <ColorSection>
                <Text
                  value={"Couleurs"}
                  padding="
          15px 0"
                />
                <Text value={"Full-stack"} />
              </ColorSection>
              <Link
                activeClass="active"
                to="footer"
                spy={true}
                smooth={true}
                offset={120}
                duration={2000}
              >
                <Button
                  color="transparent"
                  width="100%"
                  height="50px"
                  margin="20px 0 20px 0"
                  border="1px solid #000"
                  borderRadius="30px"
                  display="flex"
                  textAlign="center"
                  alignItems="center"
                  justifyContent="center"
                  backgroundColorHover="#000"
                  transitionHover="background-color 0.3s ease-out"
                  colorHover="#fff"
                  cursor="pointer"
                >
                  <Text value={"Ajouter à l'entreprise"} />
                </Button>
              </Link>
              <Text
                value={
                  "Le délai d’éxédition de ce produit est estimé entre 0 et 1 jours."
                }
                padding="
            0 0 10px 0"
              />
              <Text
                value={"Livraison sur-mesure dès demain sur Paris"}
                padding="
            0 0 10px 0"
              />
              <Link
                activeClass="active"
                to="test"
                spy={true}
                smooth={true}
                offset={120}
                duration={1000}
              >
                <BottomTextandDetailIconPosition>
                  <HiOutlineDotsCircleHorizontal size={"20px"} />
                  <Text value={"Détails"} margin="  16px 0  16px 15px " />
                </BottomTextandDetailIconPosition>
              </Link>
              <div id="test" />
            </BotStyledContent>
          </BotStyledHome>
        </GlobalStyledHome>
      </MediaQuery>
    </div>
  );
};

export default HomeContent;
