import React from "react";
import styled from "styled-components";
import Icon from "../../../atoms/Icon/Icon";
import Spacer from "../../../atoms/Spacer/Spacer";
import Text from "../../../atoms/Text/Text";
import JoinImg3 from "../../../../img/talents-rejoignez-3.jpg";
import { colors, size, font } from "../../../../Variables/globalStyles";

type Props = {};

const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const ThirdSection = (props: Props) => {
  return (
    <div>
      <ThirdSectionGlobalContainer>
        <ThirdSectionTopContainer>
          <Icon src={JoinImg3} height="100%" width="100%" />
        </ThirdSectionTopContainer>
        <ThirdSectionBottomContainer>
          <Text value="Débutez une carrière pleine de promesses" size={Max} />
          <Spacer height="20px" />
          <Text value="..." />
        </ThirdSectionBottomContainer>
      </ThirdSectionGlobalContainer>
    </div>
  );
};

const ThirdSectionGlobalContainer = styled.div`
  width: 100%;
  height: 100vh;
  display: grid;
  grid-template-rows: 30% auto;
  @media screen and (min-width: 1000px) {
  }
`;

const ThirdSectionTopContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  @media screen and (min-width: 1000px) {
  }
`;

const ThirdSectionBottomContainer = styled.div`
  width: 100%;
  height: 100%;
  background-color: ${Secondary};
  @media screen and (min-width: 1000px) {
  }
`;

export default ThirdSection;
