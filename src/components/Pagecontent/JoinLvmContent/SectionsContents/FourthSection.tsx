import React from "react";
import styled from "styled-components";
import Icon from "../../../atoms/Icon/Icon";
import Spacer from "../../../atoms/Spacer/Spacer";
import Text from "../../../atoms/Text/Text";
import LvmhExellence from "../../../../img/lvmhExellence.png";
import { colors, size, font } from "../../../../Variables/globalStyles";

type Props = {};

const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const FourthSection = (props: Props) => {
  return (
    <div>
      <FourthSectionGlobalContainer>
        <FourthSectionTopContainer>
          <Icon src={LvmhExellence} height="100%" width="100%" />
        </FourthSectionTopContainer>
        <FourthSectionBottomContainer>
          <Text value="UNE QUETE D'EXELLENCE" size={Max} />
          <Spacer height="20px" />
          <Text value="..." />
        </FourthSectionBottomContainer>
      </FourthSectionGlobalContainer>
    </div>
  );
};

const FourthSectionGlobalContainer = styled.div`
  width: 100%;
  height: 100vh;
  display: grid;
  grid-template-rows: 30% auto;
  @media screen and (min-width: 1000px) {
  }
`;

const FourthSectionTopContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  @media screen and (min-width: 1000px) {
  }
`;

const FourthSectionBottomContainer = styled.div`
  width: 100%;
  height: 100%;
  background-color: ${Secondary};
  @media screen and (min-width: 1000px) {
  }
`;

export default FourthSection;
