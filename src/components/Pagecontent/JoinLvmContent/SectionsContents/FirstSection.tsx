import React from "react";
import styled from "styled-components";
import Icon from "../../../atoms/Icon/Icon";
import Spacer from "../../../atoms/Spacer/Spacer";
import Text from "../../../atoms/Text/Text";
import JoinImg1 from "../../../../img/talents-rejoignez-1.jpg";
import { colors, size, font } from "../../../../Variables/globalStyles";

type Props = {};

const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const FirstSection = (props: Props) => {
  return (
    <div>
      <FirstSectionGlobalContainer>
        <FirstSectionTopContainer>
          <Icon src={JoinImg1} height="100%" width="100%" />
        </FirstSectionTopContainer>
        <FirstSectionBottomContainer>
          <Text value="Rejoignez la famille LVMH" size={Max} />
          <Spacer height="20px" />
          <Text value="..." />
        </FirstSectionBottomContainer>
      </FirstSectionGlobalContainer>
    </div>
  );
};

const FirstSectionGlobalContainer = styled.div`
  width: 100%;
  height: 100vh;
  display: grid;
  grid-template-rows: 30% auto;
  @media screen and (min-width: 1000px) {
  }
`;

const FirstSectionTopContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  @media screen and (min-width: 1000px) {
  }
`;

const FirstSectionBottomContainer = styled.div`
  width: 100%;
  height: 100%;
  background-color: ${Secondary};
  @media screen and (min-width: 1000px) {
  }
`;

export default FirstSection;
