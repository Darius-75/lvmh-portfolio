import React from "react";
import styled from "styled-components";
import Icon from "../../../atoms/Icon/Icon";
import Spacer from "../../../atoms/Spacer/Spacer";
import Text from "../../../atoms/Text/Text";
import JoinImg2 from "../../../../img/talents-rejoignez-2.jpg";
import { colors, size, font } from "../../../../Variables/globalStyles";

type Props = {};

const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const SecondSection = (props: Props) => {
  return (
    <div>
      <SecondSectionGlobalContainer>
        <SecondSectionTopContainer>
          <Icon src={JoinImg2} height="100%" width="100%" />
        </SecondSectionTopContainer>
        <SecondSectionBottomContainer>
          <Text value="Explorez pleinement votre potentiel" size={Max} />
          <Spacer height="20px" />
          <Text value="..." />
        </SecondSectionBottomContainer>
      </SecondSectionGlobalContainer>
    </div>
  );
};

const SecondSectionGlobalContainer = styled.div`
  width: 100%;
  height: 100vh;
  display: grid;
  grid-template-rows: 30% auto;
  @media screen and (min-width: 1000px) {
  }
`;

const SecondSectionTopContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  @media screen and (min-width: 1000px) {
  }
`;

const SecondSectionBottomContainer = styled.div`
  width: 100%;
  height: 100%;
  background-color: ${Secondary};
  @media screen and (min-width: 1000px) {
  }
`;

export default SecondSection;
