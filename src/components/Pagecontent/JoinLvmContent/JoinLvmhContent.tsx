import React from "react";
import MediaQuery from "react-responsive";
import styled from "styled-components";
import { colors, size, font } from "../../../Variables/globalStyles";
import LmmhTrailer from "../../../videos/THE WORLD OF LVMH.mp4";
import Line from "../../atoms/Line/Line";
import Spacer from "../../atoms/Spacer/Spacer";
import Text from "../../atoms/Text/Text";
import lvmhBackground from "../../../img/lvmhWallpaper.jpg";
import { VscChevronDown } from "react-icons/vsc";
import Button from "../../atoms/Button/Button";
import Icon from "../../atoms/Icon/Icon";
import FirstSection from "./SectionsContents/FirstSection";
import SecondSection from "./SectionsContents/SecondSection";
import ThirdSection from "./SectionsContents/ThirdSection";
import FourthSection from "./SectionsContents/FourthSection";
import HorizontalScroll from "react-scroll-horizontal";

type Props = {};

const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const JoinLvmhContent = (props: Props) => {
  return (
    <div>
      <MediaQuery maxWidth={1000}>
        <GlobalStyledContainer>
          <GlobalVideoTextsContainer>
            <Spacer height="50px" />

            <Text
              value="Pourquoi LVMH. . . ?"
              color="#fff"
              fontWeight="bold"
              size={Max}
              textDecoration="underline #fff"
              textUnderlineOffset="8px"
            />
            <Spacer height="100px" />

            <Text
              color="#fff"
              value=" 
              Avec plus de 75 Maisons prestigieuses couvrant six secteurs d’activité, LVMH est un terrain de jeu sans limites. "
            />
            <Line
              height="2px "
              width="50px"
              margin="20px 0 20px 115px"
              background="#fff"
            />

            <Text
              color="#fff"
              value=" 
               Voici les principaux points motivant mon envie de rejoindre votre entreprise afin de façonner le futur du luxe ! "
            />
            <Spacer height="100px" />

            <StatsContainer>
              <StatSection>
                <Text
                  textDecoration="underline #fff"
                  textUnderlineOffset="8px"
                  color="#fff"
                  value="75 MAISONS"
                />

                <Spacer height="50px" />
              </StatSection>

              <StatSection>
                <Text
                  textDecoration="underline #fff"
                  textUnderlineOffset="8px"
                  color="#fff"
                  value="+ 196 000 COLLABORATEURS en 2022"
                />
              </StatSection>
              <Spacer height="50px" />

              <StatSection>
                <Text
                  textDecoration="underline #fff"
                  textUnderlineOffset="8px"
                  color="#fff"
                  value="+ 190 NATIONALITÉS "
                />
              </StatSection>
            </StatsContainer>
            <Spacer height="150px" />
            <Button handleClick={""}>
              <VscChevronDown color="#fff" size={"40px"} />
            </Button>
          </GlobalVideoTextsContainer>
        </GlobalStyledContainer>
        <FourthSection />
        <FirstSection />
        <SecondSection />
        <ThirdSection />
      </MediaQuery>
      <MediaQuery minWidth={1001}>
        <GlobalStyledContainer>
          <GlobalVideoTextsContainer>
            <Text
              value="Pourquoi LVMH. . . ?"
              color="#fff"
              fontWeight="bold"
              size={Max}
              textDecoration="underline #fff"
              textUnderlineOffset="8px"
            />
            <Spacer height="50px" />
            <Text
              color="#fff"
              value=" 
              Avec plus de 75 Maisons prestigieuses couvrant six secteurs d’activité, LVMH est un terrain de jeu sans limites. Voici les principaux points motivant mon envie de rejoindre votre entreprise afin de façonner le futur du luxe ! "
            />
            <StatsContainer>
              <StatSection>
                <Text
                  textDecoration="underline #fff"
                  textUnderlineOffset="8px"
                  color="#fff"
                  value="75 MAISONS"
                />
                <Text
                  textDecoration="underline #fff"
                  textUnderlineOffset="8px"
                  color="#fff"
                  value=" MAISONS"
                />
              </StatSection>

              <StatSection>
                <Text
                  textDecoration="underline #fff"
                  textUnderlineOffset="8px"
                  color="#fff"
                  value="+ 196 000"
                />
                <Text
                  textDecoration="underline #fff"
                  textUnderlineOffset="8px"
                  color="#fff"
                  value="COLLABORATEURS "
                />
                <Text
                  textDecoration="underline #fff"
                  textUnderlineOffset="8px"
                  color="#fff"
                  value="en 2022"
                />
              </StatSection>

              <StatSection>
                <Text
                  textDecoration="underline #fff"
                  textUnderlineOffset="8px"
                  color="#fff"
                  value="+ 190 "
                />
                <Text
                  textDecoration="underline #fff"
                  textUnderlineOffset="8px"
                  color="#fff"
                  value="NATIONALITÉS"
                />
              </StatSection>
            </StatsContainer>
          </GlobalVideoTextsContainer>
        </GlobalStyledContainer>
      </MediaQuery>
    </div>
  );
};

const GlobalStyledContainer = styled.div`
  font-family: ${normal};
  display: flex;
  background-image: url(${lvmhBackground});

  align-items: center;
  height: 100vh;
  width: 100%;
  flex-direction: column;
  @media screen and (min-width: 1000px) {
    font-family: ${normal};
  }
`;

const Video = styled.video`
  @media screen and (min-width: 1000px) {
    min-width: 100%;
    min-height: 100%;
    position: fixed;
    object-fit: cover;
    z-index: -1;
  }
`;
const GlobalVideoTextsContainer = styled.div`
  z-index: 100;
  padding: 50px;
  text-align: center;

  @media screen and (min-width: 1000px) {
  }
`;
const StatsContainer = styled.div`
  @media screen and (min-width: 1000px) {
    display: flex;
    flex-direction: row;
  }
`;

const StatSection = styled.div`
  @media screen and (min-width: 1000px) {
    display: flex;
    flex-direction: column;
  }
`;

const DemoSection1 = styled.div`
  @media screen and (min-width: 1000px) {
    width: 30em;
    height: 100%;
  }
`;

const DemoSection2 = styled.div`
  @media screen and (min-width: 1000px) {
    width: 30em;
    height: 100%;
  }
`;
export default JoinLvmhContent;
