import React from "react";
import { AiFillUnlock } from "react-icons/ai";
import { RiOrganizationChart } from "react-icons/ri";
import { MdMapsHomeWork } from "react-icons/md";
import { BsCodeSlash } from "react-icons/bs";
import styled from "styled-components";
import Text from "../../atoms/Text/Text";
import { colors, font, size } from "../../../Variables/globalStyles";
import Spacer from "../../atoms/Spacer/Spacer";
import Line from "../../atoms/Line/Line";
import MediaQuery from "react-responsive";

type Props = {};

const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const GlobalSkillSection = styled.div`
  height: max-content;
  width: 100%;
  padding: 0px 30px;
  display: flex;
  flex-direction: column;
  font-family: ${normal};
  @media screen and (min-width: 1001px) {
    display: flex;
    flex-direction: row;
  }
`;
const SkillSection = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  width: 100%;

  @media screen and (min-width: 1001px) {
    display: flex;
    width: 100%;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    text-align: center;
  }
`;

const SkillsTextsDisplay = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 20px;
  width: 100%;
`;

const SkillsContent = (props: Props) => {
  return (
    <>
      <MediaQuery maxWidth={1000}>
        <GlobalSkillSection>
          <Spacer height="50px" />

          <SkillSection>
            <AiFillUnlock size={"50px"} />
            <SkillsTextsDisplay>
              <Text size={Max} value=" Sécurisé" padding="0 0 10px 0" />
              <Text value="L’utilisation de TypeScripe permet un code Sécurisé et sans surprise" />
            </SkillsTextsDisplay>
          </SkillSection>
          <Line
            background="#DFD9D9"
            width="100%"
            height="1px"
            margin=" 30px 0"
          />

          <SkillSection>
            <RiOrganizationChart size={"50px"} />
            <SkillsTextsDisplay>
              <Text size={Max} value=" Organisation " padding="0 0 10px 0" />
              <Text value="La maitrise d’outils agiles assure une organisation optimale  ainsi que la bonne comprehension des besoins du client" />
            </SkillsTextsDisplay>
          </SkillSection>
          <Line
            background="#DFD9D9"
            width="100%"
            height="1px"
            margin=" 30px 0"
          />

          <SkillSection>
            <BsCodeSlash size={"50px"} />
            <SkillsTextsDisplay>
              <Text size={Max} value="  Clean Code" padding="0 0 10px 0" />
              <Text value="Application de Good Practices et d’un code 100% réutilisable et commenté " />
            </SkillsTextsDisplay>
          </SkillSection>
          <Line
            background="#DFD9D9"
            width="100%"
            height="1px"
            margin=" 30px 0"
          />

          <SkillSection>
            <MdMapsHomeWork size={"50px"} />
            <SkillsTextsDisplay>
              <Text size={Max} value="  Click & collect" padding="0 0 10px 0" />
              <Text value="Livraison chez le client ou Remote Disponible     ... " />
              <Spacer height="50px" />
            </SkillsTextsDisplay>
          </SkillSection>
        </GlobalSkillSection>
      </MediaQuery>

      <MediaQuery minWidth={1001}>
        <Spacer height="50px" />
        <GlobalSkillSection>
          <Spacer height="50px" />

          <SkillSection>
            <AiFillUnlock size={"25%"} />

            <Text size={Max} value=" Sécurisé" padding="15px  0 10px 0" />
            <Text value="L’utilisation de TypeScripe permet un code Sécurisé et sans surprise" />
          </SkillSection>
          <Line
            background="#DFD9D9"
            width="5px"
            height="190px"
            margin=" 0 30px "
          />

          <SkillSection>
            <RiOrganizationChart size={"25%"} />

            <Text size={Max} value=" Organisation " padding="15px  0 10px 0" />
            <Text value="La maitrise d’outils agiles assure une organisation optimale  ainsi que la bonne comprehension des besoins du client" />
          </SkillSection>
          <Line
            background="#DFD9D9"
            width="5px"
            height="190px"
            margin=" 0 30px "
          />

          <SkillSection>
            <BsCodeSlash size={"25%"} />

            <Text size={Max} value="  Clean Code" padding="15px  0 10px 0" />
            <Text
              value="Application de Good Practices et d’un code 100% réutilisable et commenté "
              padding="0 0 25px 0"
            />
          </SkillSection>
          <Line
            background="#DFD9D9"
            width="5px"
            height="190px"
            margin=" 0 30px "
          />

          <SkillSection>
            <MdMapsHomeWork size={"25%"} />
            <Text
              size={Max}
              value="  Click & collect"
              padding="15px 0 10px 0"
            />
            <Text value="Livraison chez le client ou Remote Disponible  " />
            <Spacer height="50px" />
          </SkillSection>
        </GlobalSkillSection>
        <Spacer height="20px" />
      </MediaQuery>
    </>
  );
};

export default SkillsContent;
