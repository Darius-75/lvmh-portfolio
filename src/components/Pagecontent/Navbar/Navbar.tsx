import { GiHamburgerMenu } from "react-icons/gi/";
import { BsHandbag } from "react-icons/bs/";
import styled from "styled-components";
import Text from "../../atoms/Text/Text";
import { colors, size, font } from "../../../Variables/globalStyles";
import MediaQuery from "react-responsive";

type Props = {};

const { Primary, Secondary } = colors;
const { Max, Min, Normal, XLMax } = size;
const { normal } = font;

const GlobalStyledNavbar = styled.div`
  display: flex;
  position: relative;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 60px;
  padding: 15px;
  font-family: ${normal};
  @media screen and (min-width: 1001px) {
    height: 60px;
    padding: 50px;
    letter-spacing: 3px;
    background: linear-gradient(to right, ${Primary} 50%, #fff 50%);
  }
`;

// Responsive

const Navbar = (props: Props) => {
  return (
    <div>
      <MediaQuery maxWidth={1000}>
        <GlobalStyledNavbar>
          <GiHamburgerMenu size={"20px"} />
          <Text value={"LOUIS VUITTON"} fontWeight={"600"} size={Max} />
          <BsHandbag />
        </GlobalStyledNavbar>
      </MediaQuery>

      <MediaQuery minWidth={1001}>
        <GlobalStyledNavbar>
          <GiHamburgerMenu size={"20px"} />
          <Text
            value={"LOUIS VUITTON"}
            fontWeight={"600"}
            padding={"0 0 0 40px"}
            size={XLMax}
          />
          <BsHandbag />
        </GlobalStyledNavbar>
      </MediaQuery>
    </div>
  );
};

export default Navbar;
