import React, { useState } from "react";
import styled from "styled-components";
import Icon from "../../atoms/Icon/Icon";
import Line from "../../atoms/Line/Line";
import Spacer from "../../atoms/Spacer/Spacer";
import Text from "../../atoms/Text/Text";
import ProfilPic from "../../../img/ppf.png";
import { colors, font, size } from "../../../Variables/globalStyles";
import gift from "../../../img/gift.png";
import GiftSection from "../../organisms/GiftSection/GiftSection";
import Button from "../../atoms/Button/Button";
import GiftModal from "../../organisms/GiftModal/GiftModal";
import MediaQuery from "react-responsive";

type Props = {};

const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const GlobalDisplayPages = styled.div`
  height: max-content;
  display: flex;
  flex-direction: column;

  font-family: ${normal};
`;

const DetailsStyledContainer = styled.div`
  padding: 0 30px;
  font-family: ${normal};
  font-weight: 0;
`;

const DetailsDotStyledContainer = styled.ul`
  list-style-type: disc;
  padding-left: 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media screen and (min-width: 1000px) {
    flex-direction: column;
  }
`;

const ImageDisplayContainer = styled.div`
  display: flex;
  justify-content: center;
  background: ${Primary};
  width: 100%;
  margin-bottom: 50px;
`;

const FuturWrapper = styled.div`
  display: block;
`;

const DisplayDetailsAndModal = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  height: 150px;
  margin-bottom: 50px;
`;

const DisplayGlobalDotSection = styled.div`
  display: flex;
  height: 150px;
  width: 550px;
  justify-content: space-between;
`;

const DisplayLeftDotSection = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const DisplayRightDotSection = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const Aboutcontent = (props: Props) => {
  const [isOpen, setIsOpen] = useState(false);

  // @media screen and (min-width: 1000px) {
  //   display: flex;
  //   justify-content: start;
  //   align-items: center;
  // }

  return (
    <div>
      <MediaQuery maxWidth={1000}>
        <GlobalDisplayPages>
          <ImageDisplayContainer>
            <Icon src={ProfilPic} height="100%" width="50%" />
          </ImageDisplayContainer>
          <DetailsStyledContainer>
            <FuturWrapper>
              <Text
                value="Social :  "
                fontWeight="bold"
                float="left"
                padding="0 10px 0 0 "
              />
              <Text
                padding="0 0 15px 0"
                value=" Comme une vague d’émotions qui nous traverse, Amine Abbes invite à écouter son cœur et à vivre passionnément chaque instant"
              />
            </FuturWrapper>
            <FuturWrapper>
              <Text
                value="Experiences :  "
                fontWeight="bold"
                float="left"
                padding="0 10px 0 0 "
              />

              <Text
                padding="0 0 15px 0"
                value="  Les maître-ingenieur Capgemini et Estiam donnent corps à cet Ingenieur d’instinct et de passion par des signatures de Master 2 en informatique / Soft-Skills /  management de projet et par des  expériences clients."
              />
            </FuturWrapper>

            <FuturWrapper>
              <Text
                value="Diplomes :  "
                fontWeight="bold"
                float="left"
                padding="0 10px 0 0 "
              />

              <Text
                padding="0 0 15px 0"
                value=" Confectionné en précieux Certificats Google, Capgemini, Meta, Udemy en  Full-stack, management, design UI-UX et green iT . Ses  dimensions font de cette pièce exceptionnelle un choix splendide pour votre entreprise."
              />
            </FuturWrapper>

            <FuturWrapper>
              <Text
                value="Bonus :  "
                fontWeight="bold"
                float="left"
                padding="0 10px 0 0 "
              />
              <Text
                padding="0 0 15px 0"
                value=" Freelance et un compte instagram Pro  ajoutent de la profondeur à la composition. Un employé au caractère affirmé."
              />
            </FuturWrapper>

            <Spacer height="10px" />
            <Text
              fontWeight="500"
              listStyle="disc"
              padding="0 0 10px 0"
              value="Détails"
            />
            <DetailsDotStyledContainer>
              <li>
                <Text
                  padding="0 0 10px 0"
                  value="ingénieur finition désigner"
                />
              </li>
              <li>
                <Text
                  data-aos="fade-up"
                  padding="0 0 10px 0"
                  value="Signature Diplome Master gravée"
                />
              </li>
              <li>
                <Text
                  padding="0 0 10px 0"
                  value="Taille : M =  3 ~ 4 ans D’éxperience"
                />
              </li>
              <li>
                <Text
                  padding="0 0 10px 0"
                  value="Doublure en Freelance et E-commerce "
                />
              </li>

              <li>
                <Text
                  padding="0 0 10px 0"
                  value="Cette référence est  Made in France"
                />
              </li>
            </DetailsDotStyledContainer>
            <GiftSection handleClick={() => setIsOpen(true)} />
            <GiftModal
              dataAos={"fade-up"}
              open={isOpen}
              onClose={() => setIsOpen(false)}
            ></GiftModal>
          </DetailsStyledContainer>

          <GiftModal />
        </GlobalDisplayPages>
      </MediaQuery>

      {/* responsive pc */}

      <MediaQuery minWidth={1001}>
        <GlobalDisplayPages>
          <Line
            background="#DFD9D9"
            width="100%"
            height="5px"
            margin="10px 0 10px 0"
          />
          <Text value="Détails" padding="10px 0 10px 50%" />
          <Line
            background="#DFD9D9"
            width="100%"
            height="5px"
            margin="10px 0 10px 0"
          />
          <ImageDisplayContainer>
            <Icon src={ProfilPic} height="30%" width="10%" />
          </ImageDisplayContainer>

          <DetailsStyledContainer>
            <FuturWrapper>
              <Text
                value="Social :  "
                fontWeight="bold"
                float="left"
                padding="0 10px 0 0 "
              />
              <Text
                padding="0 0 15px 0"
                value=" Comme une vague d’émotions qui nous traverse, Amine Abbes invite à écouter son cœur et à vivre passionnément chaque instant"
              />
            </FuturWrapper>
            <FuturWrapper>
              <Text
                value="Experiences :  "
                fontWeight="bold"
                float="left"
                padding="0 10px 0 0 "
              />

              <Text
                padding="0 0 15px 0"
                value="  Les maître-ingenieur Capgemini et Estiam donnent corps à cet Ingenieur d’instinct et de passion par des signatures de Master 2 en informatique / Soft-Skills /  management de projet et par des  expériences clients."
              />
            </FuturWrapper>

            <FuturWrapper>
              <Text
                value="Diplomes :  "
                fontWeight="bold"
                float="left"
                padding="0 10px 0 0 "
              />

              <Text
                padding="0 0 15px 0"
                value=" Confectionné en précieux Certificats Google, Capgemini, Meta, Udemy en  Full-stack, management, design UI-UX et green iT . Ses  dimensions font de cette pièce exceptionnelle un choix splendide pour votre entreprise."
              />
            </FuturWrapper>

            <FuturWrapper>
              <Text
                value="Bonus :  "
                fontWeight="bold"
                float="left"
                padding="0 10px 0 0 "
              />
              <Text
                padding="0 0 15px 0"
                value=" Freelance et un compte instagram Pro  ajoutent de la profondeur à la composition. Un employé au caractère affirmé."
              />
            </FuturWrapper>

            <Spacer height="10px" />
            <Text
              fontWeight="500"
              listStyle="disc"
              padding="0 0 10px 0"
              value="Détails"
            />

            <DisplayDetailsAndModal>
              <DetailsDotStyledContainer>
                <Line
                  background="#DFD9D9"
                  width="120%"
                  height="1.5px"
                  margin="0 20px 10px -20px"
                />
                <DisplayGlobalDotSection>
                  <DisplayLeftDotSection>
                    <li>
                      <Text
                        padding="0 0 10px 0"
                        value="ingénieur finition désigner"
                      />
                    </li>
                    <li>
                      <Text
                        data-aos="fade-up"
                        padding="0 0 10px 0"
                        value="Signature Diplome Master gravée"
                      />
                    </li>
                    <li>
                      <Text
                        padding="0 0 10px 0"
                        value="Taille : M =  3 ~ 4 ans D’éxperience"
                      />
                    </li>
                  </DisplayLeftDotSection>
                  <DisplayRightDotSection>
                    <li>
                      <Text
                        padding="0 0 10px 0"
                        value="Doublure en Freelance et E-commerce "
                      />
                    </li>

                    <li>
                      <Text
                        padding="0 0 10px 0"
                        value="Cette référence est  Made in France"
                      />
                    </li>
                  </DisplayRightDotSection>
                </DisplayGlobalDotSection>
              </DetailsDotStyledContainer>
              <GiftSection handleClick={() => setIsOpen(true)} />
              <GiftModal
                dataAos={"fade-up"}
                open={isOpen}
                onClose={() => setIsOpen(false)}
              ></GiftModal>
            </DisplayDetailsAndModal>
          </DetailsStyledContainer>

          <GiftModal />
        </GlobalDisplayPages>
      </MediaQuery>
    </div>
  );
};

export default Aboutcontent;
