import styled from "styled-components";

type Props = {
  value?: string;
  fontStyle?: string;
  margin?: string;
  padding?: string;
  size?: string;
  color?: string;
  listStyle?: string;
  fontWeight?: string;
  float?: string;
  letterSpacing?: string;
  textDecoration?: string;
  textUnderlineOffset?: string;
};

const StyleText = styled.div<Props>`
  font-size: ${(props) => props.size};
  font-style: ${(props) => props.fontStyle};
  margin: ${(props) => props.margin};
  padding: ${(props) => props.padding};
  color: ${(props) => props.color};
  list-style-type: ${(props) => props.listStyle};
  font-weight: ${(props) => props.fontWeight};
  float: ${(props) => props.float};
  letter-spacing: ${(props) => props.letterSpacing};
  text-decoration: ${(props) => props.textDecoration};
  text-underline-offset: ${(props) => props.textUnderlineOffset};
`;

const Text = ({
  size,
  fontStyle,
  fontWeight,
  margin,
  padding,
  value,
  color,
  listStyle,
  float,
  letterSpacing,
  textDecoration,

  textUnderlineOffset,
}: Props) => {
  return (
    <StyleText
      size={size}
      margin={margin}
      fontStyle={fontStyle}
      padding={padding}
      color={color}
      listStyle={listStyle}
      fontWeight={fontWeight}
      float={float}
      letterSpacing={letterSpacing}
      textDecoration={textDecoration}
      textUnderlineOffset={textUnderlineOffset}
    >
      {value}
    </StyleText>
  );
};

export default Text;
