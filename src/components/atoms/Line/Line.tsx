import styled from "styled-components";

type Props = {
  background?: string;
  margin?: string;
  width?: string;
  height?: string;
  padding?: string;
  display?: string;
  aligneSelf?: string;
};

const StyleLine = styled.div<Props>`
  background: ${(props) => props.background};
  margin: ${(props) => props.margin};
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  padding: ${(props) => props.padding};
  display: ${(props) => props.display};
  align-self: ${(props) => props.aligneSelf};
`;

const Line = ({
  background,
  margin,
  height,
  width,
  padding,
  display,
  aligneSelf,
}: Props) => {
  return (
    <StyleLine
      background={background}
      margin={margin}
      height={height}
      width={width}
      padding={padding}
      display={display}
      aligneSelf={aligneSelf}
    />
  );
};

export default Line;
