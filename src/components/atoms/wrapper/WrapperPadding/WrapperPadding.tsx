import React from "react";
import styles from "./wrapperPadding.module.scss";

type Props = {
  children: React.ReactNode;
  padding?: string;
};

const WrapperPadding = ({ children, padding }: Props) => {
  return (
    <div className={styles.WrapperPadding} style={{ padding }}>
      {children}
    </div>
  );
};

export default WrapperPadding;
