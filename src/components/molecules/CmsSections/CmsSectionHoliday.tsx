import React from "react";
import styled from "styled-components";
import Button from "../../atoms/Button/Button";
import Icon from "../../atoms/Icon/Icon";
import Text from "../../atoms/Text/Text";
import MediaQuery from "react-responsive";
import holiday from "../../../videos/The Holiday Season with Louis Vuitton.mp4";
import Spacer from "../../atoms/Spacer/Spacer";
import { colors, font, size } from "../../../Variables/globalStyles";
import GlobalPage from "../../../Pages/GlobalPage";
import { useNavigate } from "react-router-dom";
import { CgScreen } from "react-icons/cg";

type Props = {};

const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const CmsSectionHoliday = (props: Props) => {
  let navigate = useNavigate();
  const HomePageRedirection = () => {
    navigate("/");
  };
  return (
    <>
      <MediaQuery maxWidth={1000}>
        <GlobalSection>
          <RightSection>
            <Video src={holiday} autoPlay loop muted />
          </RightSection>
          <LeftSection>
            <LeftElements>
              <Spacer height="30px" />
              <Text value="LES FÊTES LOUIS VUITTON" fontWeight="bold" />
              <Spacer height="50px" />

              <Text
                color="#3f3f3f"
                value="Cette année, Louis Vuitton vous invite à voyager dans un monde féérique, avec en vedette Vivienne, la mascotte de la Maison et son ami Teddy. Explorez les différents univers, où les créations Louis Vuitton prennent vie."
              />
              <Spacer height="50px" />
              <Button
                display="flex"
                alignItems="center"
                width="max-content"
                cursor="pointer"
                handleClick={() =>
                  window.open(
                    "https://fr.louisvuitton.com/fra-fr/histoires/holiday?utm_source=linkedin&utm_medium=social&utm_campaign=PUBL_HOLIDAYS_LK_FR_FRA_AW_20221110_20221114_UNI_DYN",
                    "_blank"
                  )
                }
              >
                <Text
                  value="official Website"
                  padding="0 5px 0 0"
                  fontWeight="bold"
                />
                <CgScreen />
              </Button>
              <Spacer height="50px" />

              <ButtonRedirection
                onClick={() =>
                  window.open(
                    "https://fr.louisvuitton.com/fra-fr/histoires/twist?utm_source=youtube&utm_medium=social&utm_campaign=PUBL_TWIST_YT_FR_FRA_AW_20220927_20220929_WOM_LG",
                    "_blank"
                  )
                }
              >
                <Text value="Decouvrir" />
              </ButtonRedirection>
              <Spacer height="30px" />
            </LeftElements>
          </LeftSection>
          <Spacer height="13px" />

          <ScollBarSection onClick={HomePageRedirection}>Home</ScollBarSection>
        </GlobalSection>
      </MediaQuery>

      <MediaQuery minWidth={1001}>
        <GlobalSection>
          <RightSection>
            <VideoContainer>
              <Video
                src={holiday}
                width="100%"
                height="100%"
                autoPlay
                loop
                muted
              />
            </VideoContainer>
          </RightSection>
          <LeftSection>
            <LeftElements>
              <Spacer height="200px" />
              <Spacer height="30px" />
              <Text value="LES FÊTES LOUIS VUITTON" fontWeight="bold" />
              <Spacer height="50px" />

              <Text
                color="#3f3f3f"
                value="Cette année, Louis Vuitton vous invite à voyager dans un monde féérique, avec en vedette Vivienne, la mascotte de la Maison et son ami Teddy. Explorez les différents univers, où les créations Louis Vuitton prennent vie."
              />
              <Spacer height="50px" />
              <Button
                display="flex"
                alignItems="center"
                width="max-content"
                cursor="pointer"
                handleClick={() =>
                  window.open(
                    "https://fr.louisvuitton.com/fra-fr/histoires/holiday?utm_source=linkedin&utm_medium=social&utm_campaign=PUBL_HOLIDAYS_LK_FR_FRA_AW_20221110_20221114_UNI_DYN",
                    "_blank"
                  )
                }
              >
                <Text
                  value="official Website"
                  padding="0 5px 0 0"
                  fontWeight="bold"
                />
                <CgScreen />
              </Button>
              <Spacer height="50px" />

              <ButtonRedirection
                onClick={() =>
                  window.open(
                    "https://fr.louisvuitton.com/fra-fr/histoires/twist?utm_source=youtube&utm_medium=social&utm_campaign=PUBL_TWIST_YT_FR_FRA_AW_20220927_20220929_WOM_LG",
                    "_blank"
                  )
                }
              >
                <Text value="Decouvrir" />
              </ButtonRedirection>
              <Spacer height="30px" />
            </LeftElements>
          </LeftSection>

          {/* <Spacer height="13px" />

          <ScollBarSection>▽</ScollBarSection> */}
        </GlobalSection>
      </MediaQuery>
    </>
  );
};
const GlobalSection = styled.div`
  width: 100%;
  background-color: ${Primary};
  display: flex;
  flex-direction: column;
  @media screen and (min-width: 1000px) {
    display: grid;
    height: 100%;
    grid-template-columns: 65% 35%;
    overflow: hidden;
  }
`;

const Video = styled.video`
  width: 100%;
  overflow: hidden;
  @media screen and (min-width: 1001px) {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    object-fit: cover;
    overflow: hidden;
  }
`;

const VideoContainer = styled.div`
  @media screen and (min-width: 1001px) {
    position: relative;
    height: 0;
    padding-bottom: 75%;
    overflow: hidden;
  }
`;

const LeftSection = styled.div`
  padding: 15px;
  @media screen and (min-width: 1001px) {
    width: 100%;
    padding: 0px 80px;
  }
`;

const RightSection = styled.div`
  @media screen and (min-width: 1000px) {
    width: 100%;
    height: 100%;
    object-fit: cover;
    overflow: hidden;
    background-color: #fff;
  }
`;

const ScollBarSection = styled.div`
  padding: 5px 0 5px 0;
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  color: 000;
  background-color: transparent; /* couleur de départ transparente */
  transition: background-color 0.5s ease-out;
  cursor: pointer;
  z-index: 1;
  &:hover {
    color: #000;
    background-color: ${Secondary}; /* couleur de fin */
    background-image: linear-gradient(to bottom, transparent, ${Secondary});
  }
`;

const ButtonRedirection = styled.div`
  width: max-content;
  padding: 5px;
  border: 2px solid;
  cursor: pointer;
  transition-duration: 0.4s; /* Durée de la transition */
  transition-property: background-color, color;
  &:hover {
    background-color: #555555; /* Couleur de fond au survol */
    color: white; /* Couleur de texte au survol */
  }
`;
const LeftElements = styled.div``;

export default CmsSectionHoliday;
