import React from "react";
import styled from "styled-components";
import Button from "../../atoms/Button/Button";
import Icon from "../../atoms/Icon/Icon";
import Text from "../../atoms/Text/Text";
import MediaQuery from "react-responsive";
import seelv from "../../../videos/An Inside Look at the SEE LV Exhibition in Tokyo  LOUIS VUITTON (2).mp4";
import Spacer from "../../atoms/Spacer/Spacer";
import { colors, font, size } from "../../../Variables/globalStyles";
import { CgScreen } from "react-icons/cg";

type Props = {};

const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const CmsSectionSeelv = (props: Props) => {
  return (
    <>
      <MediaQuery maxWidth={1000}>
        <GlobalSection>
          <RightSection>
            <Video src={seelv} autoPlay loop muted />
          </RightSection>
          <LeftSection>
            <LeftElements>
              <Spacer height="30px" />
              <Text value="SEE LV" fontWeight="bold" />
              <Spacer height="50px" />

              <Text
                color="#3f3f3f"
                value="Louis Vuitton présente SEE LV, un voyage immersif couvrant plus de 160 ans d'histoire de la Maison au Tokyo Midtown Grass Square."
              />
              <Spacer height="50px" />
              <Button
                display="flex"
                alignItems="center"
                width="max-content"
                cursor="pointer"
                handleClick={() =>
                  window.open(
                    "https://eu.louisvuitton.com/eng-e1/magazine/articles/see-lv-tokyo#",
                    "_blank"
                  )
                }
              >
                <Text
                  value="official Website"
                  padding="0 5px 0 0"
                  fontWeight="bold"
                />
                <CgScreen />
              </Button>
              <Spacer height="50px" />

              <ButtonRedirection
                onClick={() =>
                  window.open(
                    "https://fr.louisvuitton.com/fra-fr/histoires/twist?utm_source=youtube&utm_medium=social&utm_campaign=PUBL_TWIST_YT_FR_FRA_AW_20220927_20220929_WOM_LG",
                    "_blank"
                  )
                }
              >
                <Text value="Decouvrir" />
              </ButtonRedirection>
              <Spacer height="30px" />
            </LeftElements>
          </LeftSection>
          <Spacer height="13px" />

          <ScollBarSection>▽</ScollBarSection>
        </GlobalSection>
      </MediaQuery>

      <MediaQuery minWidth={1001}>
        <GlobalSection>
          <LeftSection>
            <LeftElements>
              <Spacer height="200px" />
              <Text value="SEE LV" fontWeight="bold" />
              <Spacer height="50px" />

              <Text
                color="#3f3f3f"
                value="Louis Vuitton présente SEE LV, un voyage immersif couvrant plus de 160 ans d'histoire de la Maison au Tokyo Midtown Grass Square."
              />
              <Spacer height="50px" />
              <Button
                display="flex"
                alignItems="center"
                width="max-content"
                cursor="pointer"
                handleClick={() =>
                  window.open(
                    "https://eu.louisvuitton.com/eng-e1/magazine/articles/see-lv-tokyo#",
                    "_blank"
                  )
                }
              >
                <Text
                  value="official Website"
                  padding="0 5px 0 0"
                  fontWeight="bold"
                />
                <CgScreen />
              </Button>
              <Spacer height="50px" />

              <ButtonRedirection
                onClick={() =>
                  window.open(
                    "https://fr.louisvuitton.com/fra-fr/histoires/twist?utm_source=youtube&utm_medium=social&utm_campaign=PUBL_TWIST_YT_FR_FRA_AW_20220927_20220929_WOM_LG",
                    "_blank"
                  )
                }
              >
                <Text value="Decouvrir" />
              </ButtonRedirection>
              <Spacer height="30px" />
            </LeftElements>
          </LeftSection>
          <RightSection>
            <VideoContainer>
              <Video
                src={seelv}
                width="100%"
                height="100%"
                autoPlay
                loop
                muted
              />
            </VideoContainer>
          </RightSection>
          {/* <Spacer height="13px" />

          <ScollBarSection>▽</ScollBarSection> */}
        </GlobalSection>
      </MediaQuery>
    </>
  );
};
const GlobalSection = styled.div`
  width: 100%;
  background-color: ${Primary};
  display: flex;
  flex-direction: column;
  @media screen and (min-width: 1000px) {
    display: grid;
    height: 100%;
    grid-template-columns: 35% 65%;
    overflow: hidden;
  }
`;

const Video = styled.video`
  width: 100%;
  overflow: hidden;
  @media screen and (min-width: 1001px) {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    object-fit: cover;
    overflow: hidden;
  }
`;

const VideoContainer = styled.div`
  @media screen and (min-width: 1001px) {
    position: relative;
    height: 0;
    padding-bottom: 75%;
    overflow: hidden;
  }
`;

const LeftSection = styled.div`
  padding: 15px;
  @media screen and (min-width: 1001px) {
    width: 100%;
    padding: 0px 80px;
  }
`;

const RightSection = styled.div`
  @media screen and (min-width: 1000px) {
    width: 100%;
    height: 100%;
    object-fit: cover;
    overflow: hidden;
    background-color: #fff;
  }
`;

const ScollBarSection = styled.div`
  padding: 5px 0 5px 0;
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  color: 000;
  background-color: transparent; /* couleur de départ transparente */
  transition: background-color 0.5s ease-out;
  cursor: pointer;
  z-index: 1;
  &:hover {
    color: #000;
    background-color: ${Secondary}; /* couleur de fin */
    background-image: linear-gradient(to bottom, transparent, ${Secondary});
  }
`;

const ButtonRedirection = styled.div`
  width: max-content;
  padding: 5px;
  border: 2px solid;
  cursor: pointer;
  transition-duration: 0.4s; /* Durée de la transition */
  transition-property: background-color, color;
  &:hover {
    background-color: #555555; /* Couleur de fond au survol */
    color: white; /* Couleur de texte au survol */
  }
`;

const LeftElements = styled.div``;

export default CmsSectionSeelv;
