import styled from "styled-components";
import styles from "./wrapper.module.scss";

const StyledFlex = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  justify-content: space-between;
`;

type Props = {
  children?: any;
  width?: string;
  margin?: string;
  padding?: string;
  height?: string;
  flexDirection?: "row" | "row-reverse" | "column" | "column-reverse";
};

const Flex = ({
  children,
  width,
  margin,
  padding,
  height,
  flexDirection,
}: Props) => {
  return (
    <StyledFlex style={{ width, margin, padding, height, flexDirection }}>
      {children}
    </StyledFlex>
  );
};

export default Flex;
