import styled from "styled-components";

const StyledGrid3 = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  gap: 10px;
  margin: 10px;
  align-items: center;
  height: 100%;

  @media screen and (max-width: "1476px") {
    grid-template-columns: 1fr 1fr;
  }
  @media screen and (max-width: "1025px") {
    grid-template-columns: 1fr;
  }
`;

type Props = {
  children?: any;
};

const Grid3 = (props: Props) => {
  return <StyledGrid3>{props.children}</StyledGrid3>;
};

export default Grid3;
