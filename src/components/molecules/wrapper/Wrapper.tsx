import styled from "styled-components";

const StyledWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  padding: 30px;
  align-content: center;
  margin: 0 0 0px 0;
`;

type Props = {
  children?: any;
  aos?: string;
};

const Wrapper = ({ aos, children }: Props) => {
  return <StyledWrapper data-aos={aos}>{children}</StyledWrapper>;
};

export default Wrapper;
