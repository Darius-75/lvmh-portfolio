import styled from "styled-components";

const StyledFlex = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  justify-content: space-between;
`;

type Props = {
  children: any;
  width?: string;
  margin?: string;
  padding?: string;
  height?: string;
  aos?: string;
};

const FlexCenter = ({
  children,
  width,
  margin,
  padding,
  height,
  aos,
}: Props) => {
  return (
    <StyledFlex data-aos={aos} style={{ width, margin, padding, height }}>
      {children}
    </StyledFlex>
  );
};

export default FlexCenter;
