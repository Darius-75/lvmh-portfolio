import styled from "styled-components";

const StyledWrapperCenter = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 90vh;
`;

type Props = {
  children: any;
  aos?: any;
};

const WrapperCenter = ({ children, aos }: Props) => {
  return <StyledWrapperCenter data-aos={aos}>{children}</StyledWrapperCenter>;
};

export default WrapperCenter;
