import React, { useState } from "react";
import Icon from "../../atoms/Icon/Icon";
import Line from "../../atoms/Line/Line";
import Text from "../../atoms/Text/Text";
import { colors, font, size } from "../../../Variables/globalStyles";
import styled from "styled-components";
import gift from "../../../img/gift.png";
import { IoIosArrowForward } from "react-icons/io";
import Spacer from "../../atoms/Spacer/Spacer";
import MediaQuery from "react-responsive";

type Props = {
  handleClick?: any;
  dataAos?: any;
};

const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const GiftDisplaySection = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media screen and (min-width: 1000px) {
    height: 150px;
    width: 100%;
  }
`;

const GiftTitlesDisplaySection = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const StyleButton = styled.div<Props>``;
//
const GiftSection = ({ handleClick, dataAos }: Props) => {
  return (
    <>
      <MediaQuery maxWidth={1000}>
        <StyleButton onClick={handleClick} data-aos={dataAos}>
          <Line background={Primary} width="100%" height="1px" />
          <GiftDisplaySection>
            <Icon
              src={gift}
              height="20%"
              width="20%"
              padding="10px 10px 10px 0"
            />
            <GiftTitlesDisplaySection>
              <Text fontWeight="400" value="L’ART D’OFFRIR" size={Normal} />
              <Text
                color={"grey"}
                fontWeight={"lighter"}
                value="Toutes les Compétences Amine Abbes sont emballées dans un emballage d'atouts iconique..."
                size={Min}
              />
            </GiftTitlesDisplaySection>
            <IoIosArrowForward size={"25px"} />
          </GiftDisplaySection>
          <Line
            background={Primary}
            width="100%"
            height="1px"
            margin=" 0 0 20px 0"
          />
        </StyleButton>
      </MediaQuery>

      <MediaQuery minWidth={1001}>
        <StyleButton onClick={handleClick} data-aos={dataAos}>
          <Line background={Primary} width="100%" height="1.6px" />
          <GiftDisplaySection>
            <Icon
              src={gift}
              height="100%"
              width="20%"
              padding="10px 10px 10px 0"
            />
            <GiftTitlesDisplaySection>
              <Text fontWeight="400" value="L’ART D’OFFRIR" size={Normal} />
              <Text
                color={"grey"}
                fontWeight={"lighter"}
                value="Toutes les Compétences Amine Abbes sont emballées dans un emballage d'atouts iconique..."
                size={Min}
              />
            </GiftTitlesDisplaySection>
            <IoIosArrowForward size={"25px"} />
          </GiftDisplaySection>
          <Line
            background={Primary}
            width="100%"
            height="1.6px"
            margin=" 0 0 20px 0"
          />
        </StyleButton>
      </MediaQuery>
    </>
  );
};

export default GiftSection;
