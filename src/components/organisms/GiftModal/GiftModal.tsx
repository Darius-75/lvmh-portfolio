import React from "react";
import styled from "styled-components";
import Button from "../../atoms/Button/Button";
import ReactDOM from "react-dom";
import { colors, font, size } from "../../../Variables/globalStyles";
import Icon from "../../atoms/Icon/Icon";
import Text from "../../atoms/Text/Text";
import { RxCross1 } from "react-icons/rx";
import lvGiftImg from "../../../img/lvGiftImg.png";
import Line from "../../atoms/Line/Line";
import MediaQuery from "react-responsive";

const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
const { normal } = font;

const GlobalModalContent = styled.div`
  position: fixed;
  top: 10%;
  background-color: #fff;
  z-index: 1000;
  height: 90%;
  width: 100%;
  border-top-left-radius: 15px;
  border-top-right-radius: 15px;
  font-family: ${normal};
  @media screen and (min-width: 1001px) {
    position: fixed;
    height: 20%;
    width: 35%;
    margin-left: 50%;
    transform: translate(-50%, 0%);
    border-radius: 15px;
  }
`;

const OverStyles = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #1d1e1e;
  opacity: 0.9;
  z-index: 1000;
`;

const TopContentSection = styled.div`
  display: flex;
  justify-content: space-between;
  width: 20%;
  padding: 20px 30px;
  height: 60px;
  box-shadow: 1px ${Primary};
`;

const TopLine = styled.div`
  display: flex;
  height: 60px;
  margin-top: -20px;
  margin-left: 250px;
`;

const HeaderGift = styled.div`
  z-index: 10001;
  position: fixed;
`;
const TitleContainer = styled.div`
  width: max-content;
  height: max-content;
`;
const CrossIconContainer = styled.div`
  padding: 0 0 0 15px;
`;

type Props = {
  dataAos?: any;
};
const portalDiv = document.getElementById("portal")!;
const GiftModal = ({ open, onClose, dataAos }: any) => {
  if (!open) return null;

  return ReactDOM.createPortal(
    <>
      <MediaQuery maxWidth={1000}>
        <OverStyles />
        <GlobalModalContent data-aos={dataAos}>
          <TopContentSection>
            <TitleContainer>
              <Text fontWeight="bold" size={Max} value="LVMH " />
            </TitleContainer>
            <TopLine>
              <Line height="60px" background={Secondary} width="2px" />
            </TopLine>
            <CrossIconContainer>
              <RxCross1 onClick={onClose} />
            </CrossIconContainer>
          </TopContentSection>
          <HeaderGift>
            <Icon src={lvGiftImg} height="75%" width="100%" />
            <Text
              fontWeight="200"
              padding="20px"
              value="Toutes les commandes Louis Vuitton sont emballées dans un coffret cadeau Louis Vuitton iconique.
Le coffret cadeau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une couleur baptisée « Safran Impérial » qui est relevé de bleu, couleur complémentaire historique déjà utilisée pour les premières personnalisations dès 1854 mais aussi depuis de nombreuses années pour les rubans de la Maison."
            />
          </HeaderGift>
        </GlobalModalContent>
      </MediaQuery>

      <MediaQuery minWidth={1001}>
        <OverStyles />
        <GlobalModalContent data-aos={dataAos}>
          <TopContentSection>
            <Text fontWeight="bold" size={Max} value="L'ART D'OFFRIR" />

            <TopLine>
              <Line height="60px" background={Secondary} width="2px" />
            </TopLine>
            <RxCross1 onClick={onClose} />
          </TopContentSection>
          <HeaderGift>
            <Icon src={lvGiftImg} height="20px  " width="100%" />
            <Text
              fontWeight="200"
              padding="20px"
              value="Toutes les commandes Louis Vuitton sont emballées dans un coffret cadeau Louis Vuitton iconique.
Le coffret cadeau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une coulau Louis Vuitton se décline dans une couleur baptisée « Safran Impérial » qui est relevé de bleu, couleur complémentaire historique déjà utilisée pour les premières personnalisations dès 1854 mais aussi depuis de nombreuses années pour les rubans de la Maison."
            />
          </HeaderGift>
        </GlobalModalContent>
      </MediaQuery>
    </>,
    portalDiv
  );
};

export default GiftModal;
