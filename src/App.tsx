import "./App.css";
import GlobalPage from "./Pages/GlobalPage";
import GridVideos from "./Pages/GridVideos";

import {
  BrowserRouter as Router,
  Routes as Switch,
  Route,
} from "react-router-dom";
import JoinLvmhPage from "./components/Pagecontent/JoinLvmContent/JoinLvmhContent";
import JoinLvmhContainer from "./components/JoinLvmhContainer";

type Props = {};

const routes = (
  <>
    <Route index element={<GlobalPage />} />;
    <Route path="/cms" element={<GridVideos />} />;
    <Route path="/test" element={<JoinLvmhContainer />} />;
    <Route path="/joinLvmh" element={<JoinLvmhPage />} />;
  </>
);

const App = (props: Props) => {
  return (
    <div style={{ height: "100vh" }}>
      <Router>
        <Switch>{routes}</Switch>
      </Router>
    </div>
  );
};

export default App;
