import { createGlobalStyle } from "styled-components";
import  intrepideregular1 from "./fonts/intrepid-webfont.woff";
import  intrepideregular2 from "./fonts/intrepid-webfont.woff2";



const FontStyles = createGlobalStyle`
@font-face {
  font-family: 'intrepidregular';
  src: url(${intrepideregular1})
      format("woff2"),
      url(${intrepideregular2})
      format("woff");

}
`;


export const font = {
  normal:  'intrepidregular',
  light:  'intrepidlight'
 }

export const colors = {
 Primary: '#E7E5E4',
 Secondary: '#f6f5f3',
}

export const size = {
 XLMax: '32px',
 Max: '24px',
 Normal: '16px',
 Min: '14px',
}


export default FontStyles;

